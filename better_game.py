from random import randint

# set up the variables

name = input('Hi! What is your name? ')
# can replace guess number by using the iterating variable of the for statement

for guess in range(5):
    print("Guess ", guess + 1,": ", name, " were you born in ",
          randint(1,12), "/", randint(1924, 2004), "?", sep="")

# get response

    response = input("yes or no: ")

# respond based on that answer

    if response == "yes":
        print("I knew it!")
        exit()
    elif response == "no" and guess == 4:
        print("Goodbye dummy have a happy birthday...")
    else:
        print('Drat! Lemme try again!')
